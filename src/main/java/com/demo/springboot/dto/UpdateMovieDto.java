package com.demo.springboot.dto;

public class UpdateMovieDto {
    private Integer movieId;
    private Integer year;
    private String title;

    public UpdateMovieDto() {
    }

    public Integer getMovieId() {
        return movieId;
    }

    public Integer getYear() {
        return year;
    }

    public String getTitle() {
        return title;
    }
}
