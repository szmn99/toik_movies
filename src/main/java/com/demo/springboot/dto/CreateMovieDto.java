package com.demo.springboot.dto;

public class CreateMovieDto {
    private Integer movieId;
    private Integer year;
    private String title;
    private String image;

    public CreateMovieDto() {
    }

    public Integer getMovieId() {
        return movieId;
    }

    public Integer getYear() {
        return year;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }
}
