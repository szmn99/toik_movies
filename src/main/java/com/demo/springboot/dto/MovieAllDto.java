package com.demo.springboot.dto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MovieAllDto {

    private final MovieListDto movies;

    public MovieAllDto(){
        List<MovieDto> moviesList = new ArrayList<>();
        moviesList.add(new MovieDto(1,
                "Piraci z Krzemowej Doliny",
                1999,
                "https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg")
        );
        movies = new MovieListDto(moviesList);
    }

    public MovieListDto getMovies(){
        return movies;
    }
}