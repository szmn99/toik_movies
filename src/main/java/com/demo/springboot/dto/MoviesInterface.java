package com.demo.springboot.dto;

import com.demo.springboot.dto.MovieListDto;

public interface MoviesInterface {
    void MovieApiController();
    MovieListDto getMovies();
}